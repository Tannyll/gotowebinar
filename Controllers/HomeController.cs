﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Runtime.Serialization.Json;
using System.Text;
using System.Web;
using System.Web.Mvc;
using LogMeIn.GoToCoreLib.Api;
using RestSharp;

namespace GoToWebinar.Controllers
{
    public class HomeController : Controller
    {
        public ActionResult Auth()
        {
            LogMeInProvider.Auth();

            return Redirect(LogMeInProvider.RedirectUrl);
        }

        public ActionResult CallBack()
        {
            var tokenResponse = LogMeInProvider.TokenResponse(Request.Url);

            ViewBag.Token = tokenResponse.access_token;
            ViewBag.Expires = tokenResponse.expires_in;
            return View();
        }
    }
}