﻿using System;
using System.Configuration;
using LogMeIn.GoToCoreLib.Api;
using LogMeIn.GoToCoreLib.Api.Model;

namespace GoToWebinar
{
    public static class LogMeInProvider
    {
        private static readonly string ClientId = ConfigurationManager.AppSettings["LogMeInClientId"];
        private static readonly string ClientSecret = ConfigurationManager.AppSettings["LogMeInClientSecret"];
        public static string RedirectUrl = "https://api.getgo.com/oauth/v2/";

        public static OAuth2Api Auth()
        {
            var authApi = new OAuth2Api(ClientId, ClientSecret);
            RedirectUrl = authApi.GetOAuth2AuthorisationUrl(ClientId);
            return authApi;
        }

        public static TokenResponse TokenResponse(Uri uri)
        {
            var auth = Auth();

            var responseKey = auth.GetResponseKey(uri);
            var tokenResponse = auth.GetAccessTokenResponse(responseKey);

            return tokenResponse;
        }
    }
}